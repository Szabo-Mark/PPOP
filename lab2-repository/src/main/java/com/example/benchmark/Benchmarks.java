package com.example.benchmark;

import com.example.model.Order;
import com.example.repository.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1000, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5,  time = 1000, timeUnit = TimeUnit.MILLISECONDS)
@Fork(value = 1, jvmArgs = {"-Xmx4G"})
public class Benchmarks {
    private static final List<Order> orders = new ArrayList<>(List.of(
            new Order(1, 1, 1),
            new Order(10, 10, 10),
            new Order(3, 3, 3),
            new Order(5, 5, 5)));


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Benchmarks.class.getSimpleName())
//                .addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Benchmark
    public ArrayListBased benchmark_add_ArrayList(ArrayListBased state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public HashSetBased benchmark_add_HashSet(HashSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public TreeSetBased benchmark_add_TreeSet(TreeSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public ConcurrentHashMap benchmark_add_ConcurrentHashMap(ConcurrentHashMap state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public HashObjSet benchmark_add_HashObjSet(HashObjSet state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public MutableList benchmark_add_MutableList(MutableList state) {
        orders.forEach(order -> state.repository.add(order));
        return state;
    }

    @Benchmark
    public ArrayListBased benchmark_contains_ArrayList(ArrayListBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public HashSetBased benchmark_contains_HashSet(HashSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public TreeSetBased benchmark_contains_TreeSet(TreeSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public ConcurrentHashMap benchmark_contains_ConcurrentHashMap(ConcurrentHashMap state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public HashObjSet benchmark_contains_HashObjSet(HashObjSet state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public MutableList benchmark_contains_MutableList(MutableList state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.contains(order));
        return state;
    }

    @Benchmark
    public ArrayListBased benchmark_remove_ArrayList(ArrayListBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @Benchmark
    public HashSetBased benchmark_remove_HashSet(HashSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @Benchmark
    public TreeSetBased benchmark_remove_TreeSet(TreeSetBased state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @Benchmark
    public ConcurrentHashMap benchmark_remove_ConcurrentHashMap(ConcurrentHashMap state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @Benchmark
    public HashObjSet benchmark_remove_HashObjSet(HashObjSet state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @Benchmark
    public MutableList benchmark_remove_MutableList(MutableList state) {
        orders.forEach(order -> state.repository.add(order));
        orders.forEach(order -> state.repository.remove(order));
        return state;
    }

    @State(Scope.Benchmark)
    public static class ArrayListBased {
        ArrayListBasedRepository<Order> repository = new ArrayListBasedRepository<>();
        @TearDown(Level.Iteration)
        public void teardown() {
            System.gc();
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetBased {
        HashSetBasedRepository<Order> repository = new HashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSetBased {
        TreeSetBasedRepository<Order> repository = new TreeSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMap {
        ConcurrentHashMapBasedRepository<Order> repository = new ConcurrentHashMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class MutableList {
        MutableListBasedRepository<Order> repository = new MutableListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class HashObjSet {
        HashObjSetBasedRepository<Order> repository = new HashObjSetBasedRepository<>();
    }
}

