package com.example.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final Map<T, Integer> map = new ConcurrentHashMap<>();
    private int count = 0;

    @Override
    public void add(T t) {
        map.put(t, count);
        count++;
    }

    @Override
    public boolean contains(T t) {
        return map.containsKey(t);
    }

    @Override
    public void remove(T t) {
        map.remove(t);
    }
}
