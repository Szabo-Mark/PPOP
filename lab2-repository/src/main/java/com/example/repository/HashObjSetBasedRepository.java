package com.example.repository;

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class HashObjSetBasedRepository<T> implements InMemoryRepository<T> {
    private final HashObjSet<T> set = HashObjSets.newMutableSet();

    @Override
    public void add(T t) {
        set.add(t);
    }

    @Override
    public boolean contains(T t) {
        return set.contains(t);
    }

    @Override
    public void remove(T t) {
        set.remove(t);
    }
}
