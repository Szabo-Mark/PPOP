package com.example.repository;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class MutableListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> list = FastList.newList();

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }
}
