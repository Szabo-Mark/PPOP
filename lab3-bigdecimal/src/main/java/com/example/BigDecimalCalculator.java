package com.example;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalCalculator {
    private List<BigDecimal> bigDecimalList;

    public BigDecimalCalculator() {
        this.bigDecimalList = new ArrayList<>();
    }

    public BigDecimal sum() {
        return bigDecimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average() {
        return bigDecimalList.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(bigDecimalList.size()), new MathContext(4));
    }

    public List<BigDecimal> getTop10Percent() {
        int percentage = (int) Math.ceil(bigDecimalList.size() * 0.1);
        return bigDecimalList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(percentage)
                .collect(Collectors.toList());
    }

    public void serializeToFile(String pathToFile) {
        try (FileOutputStream fileOutputStream
                     = new FileOutputStream(pathToFile);
             ObjectOutputStream objectOutputStream
                     = new ObjectOutputStream(fileOutputStream)) {
            bigDecimalList.forEach(bigDecimal -> {
                try {
                    objectOutputStream.writeObject(bigDecimal);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void deserializeFromFile(String pathToFile) {
        try (FileInputStream fileInputStream
                     = new FileInputStream(pathToFile);
             ObjectInputStream objectInputStream
                     = new ObjectInputStream(fileInputStream)) {
            BigDecimal bigDecimal;
            while (fileInputStream.available() != 0) {
                bigDecimal = (BigDecimal) objectInputStream.readObject();
                bigDecimalList.add(bigDecimal);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<BigDecimal> getBigDecimalList() {
        return bigDecimalList;
    }

    public void setBigDecimalList(List<BigDecimal> bigDecimalList) {
        this.bigDecimalList = bigDecimalList;
    }
}
