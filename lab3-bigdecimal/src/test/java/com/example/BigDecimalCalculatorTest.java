package com.example;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigDecimalCalculatorTest {
    private static final String PATH_TO_FILE = "src/test/resources/bigdecimals";
    private final BigDecimalCalculator bigDecimalCalculator = new BigDecimalCalculator();

    private List<BigDecimal> getBigDecimals() {
        return List.of(
                new BigDecimal(1),
                new BigDecimal(2),
                new BigDecimal(3),
                new BigDecimal(4),
                new BigDecimal(5),
                new BigDecimal(6),
                new BigDecimal(7),
                new BigDecimal(8),
                new BigDecimal(9),
                new BigDecimal(10)
        );
    }


    @Test
    void testSum() {
        bigDecimalCalculator.setBigDecimalList(getBigDecimals());
        BigDecimal expectedSum = new BigDecimal(55);

        BigDecimal actualSum = bigDecimalCalculator.sum();

        assertEquals(expectedSum, actualSum);
    }

    @Test
    void testAverage() {
        bigDecimalCalculator.setBigDecimalList(getBigDecimals());
        BigDecimal sum = new BigDecimal(55);
        BigDecimal count = new BigDecimal(10);
        BigDecimal expectedAverage = sum.divide(count, new MathContext(4));

        BigDecimal actualAverage = bigDecimalCalculator.average();

        assertEquals(expectedAverage, actualAverage);
    }

    @Test
    void testGetTop10Percent() {
        bigDecimalCalculator.setBigDecimalList(getBigDecimals());
        List<BigDecimal> expected = List.of(new BigDecimal(10));

        List<BigDecimal> actual = bigDecimalCalculator.getTop10Percent();

        assertEquals(expected, actual);
    }

    @Test
    void testSerializeAndDeserialize() {
        bigDecimalCalculator.setBigDecimalList(getBigDecimals());

        bigDecimalCalculator.serializeToFile(PATH_TO_FILE);

        BigDecimalCalculator anotherBigDecimalCalculator = new BigDecimalCalculator();
        anotherBigDecimalCalculator.deserializeFromFile(PATH_TO_FILE);

        assertEquals(bigDecimalCalculator.getBigDecimalList(), anotherBigDecimalCalculator.getBigDecimalList());
    }
}