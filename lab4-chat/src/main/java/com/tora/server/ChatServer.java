package com.tora.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ChatServer implements Runnable {
    private static int port;
    private static int nextClientId = 0;
    private Map<String, Integer> nameToIdMap = new HashMap<>();
    private Map<Integer, ClientHandler> idToHandlerMap = new HashMap<>();

    public ChatServer(int port) {
        ChatServer.port = port;
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        Thread thread = new Thread(new ChatServer(1234));
        thread.start();
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            Socket clientSocket;
            System.out.println("Server is waiting for clients....");
            while (true) {
                clientSocket = serverSocket.accept();
                System.out.println("Client socket accepted: " + clientSocket);
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
                ClientHandler clientHandler =
                        new ClientHandler(this, ++nextClientId, clientSocket, dataInputStream, dataOutputStream);
                Thread clientThread = new Thread(clientHandler);
                idToHandlerMap.put(clientHandler.getId(), clientHandler);
                System.out.println("New client got id: " + clientHandler.getId() + " and was added to idToHandlerMap");
                clientThread.start();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    synchronized public Optional<ClientHandler> getClientHandler(String name) {
        Integer clientId = nameToIdMap.get(name);
        if (clientId != null) {
            ClientHandler clientHandler = idToHandlerMap.get(clientId);
            return Optional.ofNullable(clientHandler);
        }
        return Optional.empty();
    }

    synchronized public boolean addClientName(String name, Integer id) {
        for (Map.Entry<String, Integer> entry : nameToIdMap.entrySet()) {
            if (entry.getKey().equals(name) || entry.getValue().equals(id)) {
                return false;
            }
        }
        nameToIdMap.put(name, id);
        return true;
    }

    public Map<String, Integer> getNameToIdMap() {
        return nameToIdMap;
    }

    public void setNameToIdMap(Map<String, Integer> nameToIdMap) {
        this.nameToIdMap = nameToIdMap;
    }

    public Map<Integer, ClientHandler> getIdToHandlerMap() {
        return idToHandlerMap;
    }

    public void setIdToHandlerMap(Map<Integer, ClientHandler> idToHandlerMap) {
        this.idToHandlerMap = idToHandlerMap;
    }
}
