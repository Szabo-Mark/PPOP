package com.tora.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Optional;

public class ClientHandler implements Runnable {
    private final int id;
    private final ChatServer chatServer;
    private final DataInputStream dataInputStream;
    private final DataOutputStream dataOutputStream;
    private final Socket socket;
    private String name;
    private ClientHandler requestingClientHandler;
    private ClientHandler recipientClientHandler;

    public ClientHandler(ChatServer chatServer, int id, Socket socket, DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
        this.chatServer = chatServer;
        this.id = id;
        this.dataInputStream = dataInputStream;
        this.dataOutputStream = dataOutputStream;
        this.socket = socket;

    }

    @Override
    public void run() {
        handleNameGiving();
        while (true) {
            handleMessage();
        }
    }

    private void handleNameGiving() {
        boolean clientHasName = false;
        String name;
        while (!clientHasName) {
            System.out.println("ClientHandler " + id + " waiting to receive name from client");
            name = receiveMessage(this);
            System.out.println("ClientHandler " + id + " got name " + name);
            clientHasName = chatServer.addClientName(name, id);
            sendBoolean(clientHasName);
            if (clientHasName) {
                this.name = name;
            }
        }
    }

    private void handleMessage() {
        String message = receiveMessage(this);
        System.out.println(message);
        if (message.startsWith("!hello")) {
            handleHello(message);
            return;
        }
        if (message.equals("!ack")) {
            handleAck();
            return;
        }
        if (message.equals("!bye")) {
            handleBye();
            return;
        }
        handleNormalMessage(message);
    }

    private void handleHello(String message) {
        String recipient = message.substring(6).strip();
        Optional<ClientHandler> clientHandlerOptional = chatServer.getClientHandler(recipient);
        if (clientHandlerOptional.isPresent()) {
            ClientHandler recipientClientHandler = clientHandlerOptional.get();
            sendMessage(recipientClientHandler, "You've got a request from " + name + " (!ack or !bye)");
            recipientClientHandler.requestingClientHandler = this;
        } else {
            sendMessage(this, "There's no chatter with that name");
        }
    }

    private void handleAck() {
        if (requestingClientHandler == null) {
            sendMessage(this, "You dont have a request...");
        } else {
            if (recipientClientHandler != null) {
                sendMessage(recipientClientHandler, name + " left you for someone else...");
                recipientClientHandler.recipientClientHandler = null;
            }
            recipientClientHandler = requestingClientHandler;
            requestingClientHandler = null;
            sendMessage(this, "Connection with " + recipientClientHandler.name + " is open!");
            sendMessage(recipientClientHandler, "Connection with " + name + " is open!");
            recipientClientHandler.recipientClientHandler = this;
        }
    }

    private void handleBye() {
        if (requestingClientHandler != null) {
            sendMessage(requestingClientHandler, name + " declined connection..");
            sendMessage(this, "You declined connection with " + requestingClientHandler.name);
            requestingClientHandler = null;
            return;
        }
        if (recipientClientHandler == null) {
            sendMessage(this, "You don't have what connection to close...");
        } else {
            sendMessage(this, "Connection with " + recipientClientHandler + " was closed!");
            sendMessage(recipientClientHandler, "Connection with " + name + " was closed!");
            recipientClientHandler.recipientClientHandler = null;
            this.recipientClientHandler = null;
        }
    }

    private void handleNormalMessage(String message) {
        if (recipientClientHandler == null) {
            sendMessage(this, "You have to open a connection with someone first...");
        } else {
            sendMessage(recipientClientHandler, message);
        }
    }

    private void sendMessage(ClientHandler recipient, String message) {
        try {
            recipient.dataOutputStream.writeUTF(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String receiveMessage(ClientHandler recipient) {
        try {
            return recipient.dataInputStream.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void sendBoolean(Boolean booleanValue) {
        try {
            dataOutputStream.writeBoolean(booleanValue);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getId() {
        return id;
    }

    public ClientHandler getRecipientClientHandler() {
        return recipientClientHandler;
    }

    public void setRecipientClientHandler(ClientHandler recipientClientHandler) {
        this.recipientClientHandler = recipientClientHandler;
    }
}
