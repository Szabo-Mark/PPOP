package com.tora;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ChatClient implements Runnable {
    private static int port;
    private final Scanner scanner = new Scanner(System.in);
    private String name;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    public ChatClient(int port) {
        ChatClient.port = port;
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        Thread thread = new Thread(new ChatClient(1234));
        thread.start();
    }

    @Override
    public void run() {
        try {
            InetAddress ip = InetAddress.getByName("localhost");
            try (Socket socket = new Socket(ip, port)) {
                dataInputStream = new DataInputStream(socket.getInputStream());
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                handleNameGiving();

                Thread receiveThread = new Thread(getReceiveMessageRunnable());
                Thread sendThread = new Thread(getSendMessageRunnable());

                receiveThread.start();
                sendThread.start();
                while(!socket.isClosed()){
                }
            } catch (IOException e) {
                System.out.println("Could not connect to server!");
            }
        } catch (UnknownHostException unknownHostException) {
            throw new RuntimeException(unknownHostException);
        }
    }

    private void handleNameGiving() {
        boolean clientHasName = false;
        String name;
        while (!clientHasName) {
            System.out.println("Give yourself a name:");
            name = readString();
            sendMessage(name);
            clientHasName = receiveBoolean();
            if (!clientHasName) {
                System.out.println("Choose another name...");
            } else {
                this.name = name;
                System.out.println("Your name is: " + name);
            }
        }
    }

    private String readString() {
        System.out.print('>');
        return scanner.nextLine();
    }

    private void sendMessage(String message) {
        try {
            dataOutputStream.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Boolean receiveBoolean() {
        try {
            return dataInputStream.readBoolean();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String receiveString() {
        try {
            return dataInputStream.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Runnable getSendMessageRunnable() {
        return () -> {
            while (true) {
                String message = readString();
                sendMessage(message);
            }
        };
    }

    private Runnable getReceiveMessageRunnable() {
        return () -> {
            while (true) {
                // read the message sent to this client
                String message = receiveString();
                System.out.println(message);
                System.out.print('>');
            }
        };
    }
}
