package com.example;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OperationConverterTest {

    @Test
    public void testConvert_allSigns() {
        char[] signs = {'+', '-', '/', '%'};
        for (char sign : signs) {
            ArithmeticOperation operation = OperationConverter.convertArithmetic("3" + sign + "2");

            assertEquals(3, operation.getFirstNumber());
            assertEquals(2, operation.getSecondNumber());
            assertEquals(sign, operation.getSign());
        }
    }

    @Test
    public void testConvert_whenNot3Tokens_shouldThrowIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> OperationConverter.convertArithmetic("um"));

        assertEquals("Unsupported operation", exception.getMessage());
    }

    @Test
    public void testConvert_whenTokenNotInt_shouldThrowIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> OperationConverter.convertArithmetic("z+3"));

        assertEquals("Token 'z' is not an int", exception.getMessage());
    }
}