package com.example;


import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Random;

import static java.lang.Math.abs;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    private static final Random random = new Random(LocalDateTime.now().getNano());
    private final Calculator calculator = new Calculator();

    @Test
    public void testCalculate() {
        for (int i = 0; i < 100; i++) {
            int firstNumber = abs(random.nextInt());
            int secondNumber = abs(random.nextInt());

            assertEquals(firstNumber + secondNumber, calculator.calculate(firstNumber + "+" + secondNumber));
            assertEquals(firstNumber - secondNumber, calculator.calculate(firstNumber + "-" + secondNumber));
            assertEquals(firstNumber / secondNumber, calculator.calculate(firstNumber + "/" + secondNumber));
            assertEquals(firstNumber % secondNumber, calculator.calculate(firstNumber + "%" + secondNumber));
        }
    }
}