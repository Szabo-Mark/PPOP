package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ArithmeticOperationTest {

    @Test
    public void testCalculate_plusSign() {
        Operation operation = new ArithmeticOperation(1, 1, '+');

        int result = operation.getResult();

        assertEquals(1 + 1, result);
    }

    @Test
    public void testCalculate_minusSign() {
        Operation operation = new ArithmeticOperation(2, 1, '-');

        int result = operation.getResult();

        assertEquals(2 - 1, result);
    }

    @Test
    public void testCalculate_divisionSign() {
        Operation operation = new ArithmeticOperation(10, 5, '/');

        int result = operation.getResult();

        assertEquals(10 / 5, result);
    }

    @Test
    public void testCalculate_moduloSign() {
        Operation operation = new ArithmeticOperation(5, 2, '%');

        int result = operation.getResult();

        assertEquals(5 % 2, result);
    }

    @Test
    public void testCalculate_shouldThrowIllegalArgumentException() {
        Operation operation = new ArithmeticOperation(1, 1, ']');

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, operation::getResult);

        assertEquals("Unsupported sign ']'", exception.getMessage());
    }
}