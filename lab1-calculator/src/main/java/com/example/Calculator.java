package com.example;

public class Calculator {
    public int calculate(String operation) {
        Operation convertedOperation = OperationConverter.convertArithmetic(operation);
        return convertedOperation.getResult();
    }
}