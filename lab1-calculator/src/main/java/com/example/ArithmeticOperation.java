package com.example;

public class ArithmeticOperation implements Operation {
    private final int firstNumber;
    private final int secondNumber;
    private final char sign;

    public ArithmeticOperation(int firstNumber, int secondNumber, char sign) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.sign = sign;
    }

    @Override
    public int getResult() {
        int result;
        switch (sign) {
            case '+':
                result = firstNumber + secondNumber;
                break;
            case '-':
                result = firstNumber - secondNumber;
                break;
            case '/':
                result = firstNumber / secondNumber;
                break;
            case '%':
                result = firstNumber % secondNumber;
                break;
            default:
                throw new IllegalArgumentException("Unsupported sign '" + sign + "'");
        }
        return result;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public char getSign() {
        return sign;
    }
}
