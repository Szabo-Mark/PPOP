package com.example;

public class OperationConverter {
    public static ArithmeticOperation convertArithmetic(String operationString) {
        String signs = String.valueOf(Utils.SIGNS);
        String[] tokens = operationString.split("((?=[" + signs + "])|(?<=[" + signs + " ]))");
        if (tokens.length != 3) {
            throw new IllegalArgumentException("Unsupported operation");
        }
        return new ArithmeticOperation(getInt(tokens[0]), getInt(tokens[2]), tokens[1].charAt(0));
    }

    private static int getInt(String token) {
        try {
            return Integer.parseInt(token);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException("Token '" + token + "' is not an int");
        }
    }
}
