package com.example;

public interface Operation {
    int getResult();
}
